package qaopsautomationapi.teste;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;

import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.baseURI;

public class TesteBase {

    @BeforeClass
    public static void setup(){
        //       enableLoggingOfRequestAndResponseIfValidationFails(); habilita log verboso para casos de falha
        baseURI = "https://reqres.in";
        basePath = "/api";

        //Especifica que o reaquest é no formato JSON
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .build();
        //Valida que o response é no formato JSON
        RestAssured.responseSpecification = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .build();
    }
}
