package qaopsautomationapi.teste;

import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;
import org.junit.Test;
import qaopsautomationapi.dominio.Usuario;
import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.is;

public class TesteRegistro extends TesteBase {

    private static final String REGISTRA_USUARIO_ENDPOINT = "/register";
    private static final String LOGIN_USUARIO_ENDPOINT = "/login";

    @BeforeClass
    public static void setupRegistro(){
        RestAssured.responseSpecification = new ResponseSpecBuilder()
                .expectStatusCode(HttpStatus.SC_BAD_REQUEST)
                .build();
    }

    @Test
    public void testNaoEfetuaRegistroQuandoSenhaEstaFaltando(){
        Usuario usuario = new Usuario();
        usuario.setEmail("sydney@fife");
        given().
                //body("{\"email\": \"sydney@fife\"}").
                        body(usuario).
        when().
                post(REGISTRA_USUARIO_ENDPOINT).
        then().log().all().
                body("error", is ("Missing password"));
    }

    @Test
    public void testLoginNaEfetuadoQuandoSenhaEstaFaltando(){
        Usuario usuario = new Usuario();
        usuario.setEmail("sydney@fife");
        given().
            body(usuario).
        when().
            post(LOGIN_USUARIO_ENDPOINT).
        then().log().all().
            body("error", is ("Missing password"));
    }
}
