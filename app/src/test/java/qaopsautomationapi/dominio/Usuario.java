package qaopsautomationapi.dominio;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data   //getter, setter e etc
@AllArgsConstructor
@NoArgsConstructor
public class Usuario {

    private String name;
    private String job;
    private String email;




    /*
    public Usuario(){}
    //construtor
    public Usuario(String name, String job, String email) {
        this.name = name;
        this.job = job;
        this.email = email;
    }


    public String getJob() {
        return job;
    }
    public String getEmail() {
        return this.email;
    }


    //Quando for enviar algum dado, é importante fazer um set
    public void setEmail(String email){
        this.email = email;
    }
    )
   */
}
